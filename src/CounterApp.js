import React, { useState } from 'react';
import PropTypes from 'prop-types';

const CounterApp = ({
    title,
    description,
    value
  }) => {

  const [number, setNumber] = useState(value)

  const handleIncrement = () => {
    setNumber(number + 1);
  }

  const handleDecrement = () => {
    setNumber(number - 1);
  }

  const handleReset = () => {
    setNumber(value);
  }

  return (
    <>
      <h1> { title } </h1>
      <div> { description } </div>
      <h2> { number } </h2>
      <button onClick={ handleIncrement }> +1 </button>
      <button onClick={ handleDecrement }> -1 </button>
      <button onClick={ handleReset }> Reset </button>
    </>
  );
}

CounterApp.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
}

CounterApp.defaultProps = {
  description: 'Just click to see the counter working',
  value: 0,
}

export default CounterApp;