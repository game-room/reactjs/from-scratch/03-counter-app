import React from 'react';
import ReactDOM from 'react-dom';
import CounterApp from './CounterApp'
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <CounterApp
      title={'Counter App'}
      value={2}
    />
  </React.StrictMode>,
  document.getElementById('root')
);
